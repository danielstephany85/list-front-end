import React, {Component} from 'react';
import PropTypes from 'prop-types';
import ListItem from './ListItem';
import './ListContainer.scss';

class ListContainer extends Component {

    componentDidMount = () => {
        if(!this.props.list.length){
            this.props.history.push("/note");
        }
    }

    render = () =>{
        var list_items = this.props.list.map((item, i) => {
            return <ListItem key={i} history={this.props.history} item={item} removeTask={this.props.removeTask} markComplete={this.props.markComplete} toggleEdit={this.props.toggleEdit} />
        });
        return (
            <div className="content-section">
                <ul className="list-container">
                    {list_items}
                </ul>
            </div>
        );
    }
}

ListContainer.propTypes = {
    list: PropTypes.array.isRequired,
    removeTask: PropTypes.func.isRequired,
    markComplete: PropTypes.func.isRequired,
    signedIn: PropTypes.bool.isRequired,
}

export default ListContainer;