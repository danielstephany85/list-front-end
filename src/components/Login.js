import React, {Component} from 'react';

class Login extends Component {
    constructor(props){
        super(props);
        this.state = {
            email: "",
            password: "",
            passwordErr: false,
            emailErr: false,
            responseErr: false
        }
    }

    checkPass = (password) => {
        var errorName = password + "Err"
        if (this.state[password] === '') {
            this.setState({ [errorName]: "Password can not be empty" });
            return false;
        }
        return true;
    }

    checkEmail = () => {
        if (this.state.email === '') {
            this.setState({ emailErr: "Email can not be empty" });
            return false;
        }
        var emailCheck = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (!emailCheck.test(this.state.email.toLowerCase())) {
            this.setState({ emailErr: "Please enter valid email" });
            return false;
        }
        return true;
    }


    handleSubmit = (e) => {
        e.preventDefault();
        this.setState({
            passwordErr: '',
            emailErr: ''
        }, () => {
            var valid = true;
            if (!this.checkPass('password')) valid = false;
            if (!this.checkEmail()) valid = false;
            if (valid) {
                var user = {
                    email: this.state.email,
                    password: this.state.password
                }

                fetch("http://localhost:8080/api/authenticate",
                    {
                        body: JSON.stringify(user),
                        method: 'POST',
                        headers: {
                            'content-type': 'application/json'
                        },
                    })
                    .then((response) => {
                        if (response.ok) {
                            return response.json();
                        }
                        throw new Error('Network response was not ok.');
                    })
                    .then((response) => {
                        if (response.success) {
                            this.props.setUser(response.data.user, response.data.token, () => {
                                window.localStorage.setItem('userId', response.data.user._id);
                                window.localStorage.setItem('token', response.data.token);
                                if (response.data.user.list.length > 0) {
                                    this.props.history.push('/list');
                                } else {
                                    this.props.history.push('/note');
                                }
                            });
                        } else {
                            this.setState({ responseErr: response.message });
                        }
                    }).catch((e) => {
                        console.dir(e);
                    });
            } 
        });

    }

    render = () => {
        return(
            <div className="content-section">
                <div className="form-container">
                    <h2>Welcome to ListIt!</h2> 
                    <form onSubmit={(e)=>{this.handleSubmit(e)}}>
                        <div className="form-container__item">
                            <label>
                                Email
                                <input type="email" value={this.state.email} onChange={(e) => { this.setState({ email: e.target.value }); }} />
                            </label>
                            {this.state.emailErr ? <span className="error-msg">{this.state.emailErr}</span> : ""}
                        </div>
                        <div className="form-container__item">
                            <label>
                                Password
                                <input type="password" value={this.state.password} onChange={(e) => { this.setState({ password: e.target.value }); }} />
                            </label>
                            {this.state.passwordErr ? <span className="error-msg">{this.state.passwordErr}</span> : ""}
                            {this.state.responseErr ? <span className="error-msg">{this.state.responseErr}</span> : ""}
                        </div>
                        <div className="form-container__item">
                            <button className="form-btn" type="submit" >Login</button>
                        </div>
                    </form>
                </div>
            </div>
        );
    }
}

export default Login;