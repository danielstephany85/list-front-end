import React from 'react';
import { NavLink } from 'react-router-dom'

const NotFound = () => {

    return (
        <div className="content-section">
            <div className="not-found">
                <h1>404 requested content not found</h1>
                <NavLink to="/" className="main-logo">Home</NavLink>
            </div>
        </div>
    );
}

export default NotFound;