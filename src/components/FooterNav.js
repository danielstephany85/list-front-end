import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import PropTypes from 'prop-types';

class FooterNav extends Component {

    static propTypes = {
        signedIn: PropTypes.bool.isRequired,
        setSignedIn: PropTypes.func.isRequired,
    }

    render = () => {
        var noteLink = this.props.signedIn? 
            <div className="note-btn-container">
                <NavLink className={`note-btn`} to="/note">
                    <i className="fas fa-pencil-alt"></i>
                    <i className="fas fa-plus"></i>
                    <i className="fas fa-list-ul"></i>
                </NavLink>
                <NavLink className={`note-btn`} to="/list"></NavLink>
            </div>
            :
            "";

        return(
            <div className={`footer-nav`}>
                {noteLink}
                {this.props.signedIn ? <button className="nav-btn" onClick={()=>{this.props.setSignedIn(()=>{this.props.history.push('/')})}}>log out</button>:""}
            </div>
        );
    }
}

export default FooterNav;