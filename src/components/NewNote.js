import React, {Component} from 'react';
import PropTypes from 'prop-types';

class NewNote extends Component {
    constructor(props) {
        super(props);
        this.state = {
            editing: false,
            _id: undefined,
            title: "",
            body: "",
            complete: false,
            newNote: true
        }
    }

    componentDidMount = () =>{
        if(this.state.title !== '' && this.state.body !== ''){
            this.setState({newNote: false});
        }
        if (Object.keys(this.props.match.params).length !== 0) {
            var paramId = this.props.match.params.id;
            let note = this.props.list.filter((item) => {return item._id === paramId});
            note = note[0];
            if (note){
                this.setState({
                    _id: note._id,
                    title: note.title,
                    body: note.body,
                    complete: note.complete,
                    newNote: false
                });
             
            }else {
                this.props.history.push('/not-found')
            }
           
        }
    }

    handleRemove = () => {
        this.props.removeTask(this.state._id)
        this.props.history.push('/list')
    }

    editing = (value) => {
        this.setState({editingValue: value});
    }

    toggleComplete = () => {
        this.setState({complete: !this.state.complete});
        this.setState({ editing: true });
    }

    onChange = (e, valueType) => {
        let value = e.target.value
        this.setState({ [valueType]: value },()=>{
            if (this.state.title !== "" || this.state.body !== "") {
                if (!this.state.editing) {
                    this.setState({ editing: true });
                }
            }else if (this.state.title === "" && this.state.body === "") {
                this.setState({ editing: false });
            }
        });
    }

    onKeyDown = (e, valueType) => {
        if (e.keyCode === 13) { //Enter keycode
            this.onSubmit(e);
        }else {
            this.onChange(e, valueType);
        }
    }

    onSubmit = (e) => {
        if (e) e.preventDefault();
        var note = {
            title: this.state.title,
            body: this.state.body,
            complete: this.state.complete
        };
        if (this.state._id === undefined){
            this.props.addTask(note);
        }else {
            note._id = this.state._id;
            this.props.updateTask(this.state._id, note);
        }
        this.props.history.push("/list");
    }

    render = () => {
        var editing = this.state.editing ? "editing": "";
        var newNote = this.state.newNote ? "new-note": "";
        var complete = this.state.complete? "complete": "";

        return (
            <div className="content-section">
                <div className={`note-view  ${newNote} ${editing} ${complete}`}>
                    <div>
                        <form className="editable-text" onSubmit={(e) => { this.onSubmit(e) }}>
                            <div className="note-title">
                                <h3 aria-hidden="true">
                                    {this.state.title ? this.state.title : "Title"}
                                    <textarea className="note-title-input"
                                        onChange={(e) => { this.onChange(e, 'title'); }}
                                        onKeyDown={(e) => { this.onKeyDown(e, 'title');}}
                                        value={this.state.title}
                                        placeholder="Title"
                                    ></textarea>
                                </h3>
                            </div>
                            <div className="note-body">
                                <p aria-hidden="true">
                                    {this.state.body? this.state.body : "Body"}
                                    <textarea className="note-body-textarea"
                                        placeholder="body"
                                        onChange={(e) => { this.onChange(e, 'body'); }}
                                        onKeyDown={(e) => { this.onKeyDown(e, 'body') }}
                                        value={this.state.body}
                                    ></textarea>
                                </p>
                            </div>
                        </form>
                    </div>
                    <button type="submit" className="simple-btn save" onClick={(e) => { this.onSubmit(e) }}>save</button>
                    <button type="button" className="check-btn" onClick={this.toggleComplete}><i className="fas fa-check"></i></button>
                    <button type="button" className="delete-btn" onClick={this.handleRemove}><i className="fas fa-trash"></i></button>
                </div>
            </div>
        );
    }
}

NewNote.propTypes = {
    list: PropTypes.array.isRequired,
    updateTask: PropTypes.func.isRequired,
    markComplete: PropTypes.func.isRequired,
    removeTask: PropTypes.func.isRequired,
    signedIn: PropTypes.bool.isRequired,
}

export default NewNote;