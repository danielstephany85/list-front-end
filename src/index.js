import React from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
import ListApp from './ListApp';
// import { Provider } from 'react-redux';
// import { createStore, applyMiddleware } from 'redux';
// import thunk from 'redux-thunk';

ReactDOM.render(<ListApp />, document.getElementById('root'));
